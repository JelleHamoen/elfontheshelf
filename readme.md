# Beunbuddy

Beunen, verb, to create without being impeded by overt intelligence. Beunbuddy is your buddy to clear your mind from
your workshop distractions, and keep an easy overview of both your maker space and your head space, allowing you to make
stuff to your heart's content.

#### Potential features

- showcase
- project suggestions in accordance with ingredients in inv manager
- timeline/ project tracker
- File storage
- Drawing pad

#### Pages

- profile
- inventory
    - list inventory
    - add/remove/edit item
    - item/project suggestions
- projects
    - list project
    - add/remove/edit project
    - project page
        - Description
        - Timeline
        - Assets

#### Model TODO

- note
    - To workspace
    - Read/Delete per user?
- Workspace
    - Assets
    - Associated users
    - Projects
    - Public/private?
    - Permissions owner/user

#### TODO

- Media management
    - storage
    - img cropping
    - file storage
    - potential drive/dropbox coupling?
- minification, css, scss
- making pages pretty, basically the entire UI
- Item, Tool, project sharing, default tools for easy ingress
- Create "add" button when list is empty

#### Business models

- Advertisements (not google ads, proper features) (tiles?)
- Suggestion pushing
- Premium models
    - more file storage space
    - more projects/assets/notes?
    - Main selling point: Pay per workspace
        - Potential enterprise grade b2b profit
    - showcase?
        - maybe premium feature
        - push profiles/showcases?
    
todo external logging
todo fix note font
todo only make projects from user visible in new note
todo parse images with sharp and check parsing on ingest

Notes/dingen Archiving 
Amounts in assets and nice UI features