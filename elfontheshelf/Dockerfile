FROM python:3.9

# update packages
RUN apt-get -qq update
RUN pip install uwsgi

RUN mkdir /code
WORKDIR /code

# copy and install requirements first to leverage caching
COPY requirements.txt /code/
RUN pip install -r requirements.txt

# copy the actual code
COPY . /code/
#COPY ./database.sqlite /code
ARG APP_USER=appuser
RUN groupadd -r ${APP_USER} && useradd --no-log-init -r -g ${APP_USER} ${APP_USER}

EXPOSE 8000

ENV DJANGO_SETTINGS_MODULE=elfontheshelf.settings
ENV ENV_PATH=.prodenv
RUN python manage.py collectstatic --noinput

ENV UWSGI_WSGI_FILE=/code/elfontheshelf/wsgi.py

ENV UWSGI_SOCKET=:8000 UWSGI_MASTER=1 UWSGI_HTTP_AUTO_CHUNKED=1 UWSGI_HTTP_KEEPALIVE=1 UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy
ENV UWSGI_WORKERS=2 UWSGI_THREADS=4
ENV UWSGI_STATIC_MAP="/static/=/code/static/" UWSGI_STATIC_EXPIRES_URI="/static/.*\.[a-f0-9]{12,}\.(css|js|png|jpg|jpeg|gif|ico|woff|ttf|otf|svg|scss|map|txt) 315360000"

RUN chown -R $APP_USER:$APP_USER /code/

USER ${APP_USER}:${APP_USER}

CMD python manage.py makemigrations --noinput
CMD python manage.py migrate --noinput


CMD ["uwsgi", "--show-config"]


