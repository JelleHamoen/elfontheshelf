# Utility file for view helper functions
from .models import Invite
from django.contrib import messages


def check_invite_token(request, invite_token):
    db_token = Invite.objects.filter(token=invite_token).first()
    if db_token is None:
        messages.add_message(request, messages.ERROR,
                             'This token is invalid, try again with a valid token')
        return False
    if db_token.status != 'PENDING':
        if db_token.status == 'ACCEPTED':
            messages.add_message(request, messages.WARNING,
                                 'This token has already been used, please try another one')
        elif db_token.status == 'EXPIRED':
            messages.add_message(request, messages.WARNING,
                                 'This token is expired, please try another one')
        else:
            messages.add_message(request, messages.ERROR,
                                 'This token is invalid, try again with a valid token')
        return False
    if db_token.status == 'PENDING':
        if db_token.is_expired():
            # Check if expired
            db_token.status = 'EXPIRED'
            db_token.save()
            return False
        return True
    return False
