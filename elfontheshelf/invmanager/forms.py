from django import forms
from django.forms import ModelForm
from simple_history.models import HistoricalRecords
from django import forms
from .models import Project, InventoryNote, Asset, UserProfile
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class ProjectForm(ModelForm):
    change_reason = forms.CharField(label='Change description (optional)', required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'e.g. first prototype completed'}))

    class Meta:
        model = Project
        fields = ['project_title', 'project_description', 'project_tags', 'status', 'thumbnail']
        widgets = {
            'project_title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'project_description': RichTextField(

            ),
            'project_tags': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'status': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'thumbnail': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file'
                }
            ),
        }


class NoteForm(ModelForm):
    class Meta:
        model = InventoryNote
        fields = ['note_text', 'note_tags', 'status', 'project', 'asset']
        widgets = {
            'note_tags': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'note_text': forms.Textarea(
                attrs={
                    'class': 'form-control'
                }
            ),
            'status': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),

            'project': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            'asset': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),

        }


class AssetForm(ModelForm):
    change_reason = forms.CharField(label='Change description (optional)', required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'e.g. lent it to barry'}))

    class Meta:
        model = Asset
        fields = ['title', 'category', 'description', 'project', 'status', 'raw_tags', 'thumbnail']
        widgets = {
            'raw_tags': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'category': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            'status': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'description': RichTextField(

            ),
            'project': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            'asset': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            'thumbnail': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file'
                }
            ),

        }


class UpdateProfile(forms.ModelForm):
    username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_email(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(
                'This email address is already in use. Please supply a different email address.')
        return email


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website', 'thumbnail')
        widgets = {
            'thumbnail': forms.ClearableFileInput(
                attrs={
                    'class': 'form-control-file'
                }
            ),
            'website': forms.URLInput(
                attrs={
                    'class': 'form-control'
                }
            ),
        }
