from django.http import HttpResponse
from .models import Project, InventoryNote, Asset, UserProfile, Invite
from .forms import ProjectForm, NoteForm, AssetForm, UpdateProfile, UserProfileForm
from django.template import loader
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from simple_history.utils import update_change_reason
from .utils import check_invite_token


def index(request):
    if not request.user.is_authenticated:
        return render(request, 'invmanager/pages/index.html')
    latest_project_list = list(Project.objects.filter(project_author=request.user).all())
    latest_note_list = list(InventoryNote.objects.filter(note_user=request.user).all())
    latest_asset_list = list(Asset.objects.filter(user=request.user).all())
    template = loader.get_template('invmanager/lists/listall.html')
    tiles = sorted(latest_project_list + latest_note_list + latest_asset_list,
                   key=lambda item: item.last_modified, reverse=True)
    tiles = [(tile, tile.__class__.__name__) for tile in tiles]
    context = {
        'tiles': tiles,
        'title': 'Beunbuddy| Index',
        'search': True,
        'hide_bg': True,
    }
    return HttpResponse(template.render(context, request))


@login_required
def projectlist(request):
    # TODO add user
    latest_project_list = Project.objects.filter(project_author=request.user).order_by('-last_modified').all()
    template = loader.get_template('invmanager/lists/projectlist.html')
    context = {
        'latest_project_list': latest_project_list,
        'title': 'Beunbuddy| Projects',
        'search': True,
    }
    return HttpResponse(template.render(context, request))


@login_required
def notelist(request):
    latest_note_list = InventoryNote.objects.filter(note_user=request.user).order_by('-last_modified').all()
    template = loader.get_template('invmanager/lists/notelist.html')
    context = {
        'latest_note_list': latest_note_list,
        'title': 'Beunbuddy| Notes',
        'search': True,
    }
    return HttpResponse(template.render(context, request))


@login_required
def assetlist(request):
    latest_asset_list = Asset.objects.filter(user=request.user).order_by('-last_modified').all()
    template = loader.get_template('invmanager/lists/assetlist.html')
    context = {
        'latest_asset_list': latest_asset_list,
        'title': 'Beunbuddy| Assets',
        'search': True,
    }
    return HttpResponse(template.render(context, request))


@login_required
def profile(request):
    profile_data = get_object_or_404(UserProfile, user=request.user)
    return render(request, 'invmanager/pages/profile.html',
                  {'profile_data': profile_data, 'title': 'Beunbuddy| Profile'})


@login_required
def edit_profile(request):
    user = request.user
    profile_data = get_object_or_404(UserProfile, user=request.user)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = UpdateProfile(request.POST, instance=user)
        profile_form = UserProfileForm(request.POST, request.FILES, instance=profile_data)
        # check whether it's valid:
        if form.is_valid() and profile_form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            profile_form.save()
            messages.add_message(request, messages.SUCCESS, 'Profile edited successfully')
            return redirect('profile')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = UpdateProfile(instance=user)
        profile_form = UserProfileForm(instance=profile_data)

    return render(request, 'invmanager/forms/profile_edit.html', {'form': form, 'profile_form': profile_form})


@login_required
def asset_page(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    # check if user is allowed to see it
    if any([asset.user == request.user, request.user.is_superuser]):
        if asset.project:
            project = Project.objects.get(pk=asset.project.id)
        else:
            project = None
        return render(request, 'invmanager/pages/assetpage.html', {'asset': asset, 'project': project})
    else:
        messages.add_message(request, messages.WARNING, 'You are not allowed to see this page')
        return redirect('index')


@login_required
def project_page(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    if any([project.project_author == request.user, request.user.is_superuser]):
        connected_asset_list = list(
            Asset.objects.filter(user=request.user, project=project).all())
        latest_note_list = list(
            InventoryNote.objects.filter(note_user=request.user, project=project).all())
        tiles = sorted(connected_asset_list + latest_note_list, key=lambda item: item.last_modified,
                       reverse=True)
        tiles = [(tile, tile.__class__.__name__) for tile in tiles]
        return render(request, 'invmanager/pages/projectpage.html', {'project': project, 'tiles': tiles})
    else:
        messages.add_message(request, messages.WARNING, 'You are not allowed to see this page')
        return redirect('index')


@login_required
def note_page(request, note_id):
    # TODO check if user has access
    note = get_object_or_404(InventoryNote, pk=note_id)
    if any([note.note_user == request.user, request.user.is_superuser]):
        return render(request, 'invmanager/pages/notepage.html', {'note': note})
    else:
        messages.add_message(request, messages.WARNING, 'You are not allowed to see this page')
        return redirect('index')


@login_required
def tag_page(request, tag):
    latest_project_list = list(
        Project.objects.filter(project_author=request.user, project_tags__icontains=tag).all())
    latest_note_list = list(
        InventoryNote.objects.filter(note_user=request.user, note_tags__icontains=tag).all())
    latest_asset_list = list(
        Asset.objects.filter(user=request.user, raw_tags__icontains=tag).all())
    template = loader.get_template('invmanager/lists/listall.html')
    tiles = sorted(latest_project_list + latest_note_list + latest_asset_list,
                   key=lambda item: item.last_modified,
                   reverse=True)
    tiles = [(tile, tile.__class__.__name__) for tile in tiles]
    context = {
        'tiles': tiles,
        'title': 'Beunbuddy| {}'.format(tag),
        'search': True,
    }
    return HttpResponse(template.render(context, request))


@login_required
def edit_project(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ProjectForm(request.POST, request.FILES, instance=project)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            if form.cleaned_data.get('change_reason'):
                update_change_reason(project, form.cleaned_data.get('change_reason'))
            else:
                update_change_reason(project, 'project edited')
            messages.add_message(request, messages.SUCCESS, 'Project edited successfully')
            return redirect('project', project_id=project.id)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ProjectForm(instance=project)

    return render(request, 'invmanager/forms/project_edit.html', {'form': form, 'project': project})


@login_required
def edit_asset(request, asset_id):
    asset = get_object_or_404(Asset, pk=asset_id)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AssetForm(request.POST, request.FILES, instance=asset)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            if form.cleaned_data.get('change_reason'):
                update_change_reason(asset, form.cleaned_data.get('change_reason'))
            else:
                update_change_reason(asset, 'asset edited')
            if form.cleaned_data.get('change_reason'):
                update_change_reason(asset, form.cleaned_data.get('change_reason'))
            else:
                update_change_reason(asset, 'project edited')
            messages.add_message(request, messages.SUCCESS, 'Project edited successfully')
            return redirect('asset', asset_id=asset.id)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AssetForm(instance=asset)

    return render(request, 'invmanager/forms/asset_edit.html', {'form': form, 'asset': asset})


@login_required
def edit_note(request, note_id):
    note = get_object_or_404(InventoryNote, pk=note_id)
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST, instance=note)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Note edited successfully')
            return redirect('note', note_id=note.id)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm(instance=note)

    return render(request, 'invmanager/forms/note_edit.html', {'form': form, 'note': note})


@login_required
def delete_note(request, note_id):
    if request.method == 'POST':
        obj = get_object_or_404(InventoryNote, pk=note_id)
        if any([obj.note_user == request.user, request.user.is_superuser]):
            obj.delete()
            messages.add_message(request, messages.SUCCESS, 'Note {} has been deleted successfully'.format(obj))
        else:
            messages.add_message(request, messages.WARNING, 'You don\'t have permission to edit this'.format(obj))
    else:
        messages.add_message(request, messages.ERROR, 'Something went wrong')
    return redirect('index')


@login_required
def delete_project(request, project_id):
    if request.method == 'POST':
        obj = get_object_or_404(Project, pk=project_id)
        if any([obj.project_author == request.user, request.user.is_superuser]):
            obj.delete()
            messages.add_message(request, messages.SUCCESS, 'Project {} has been deleted successfully'.format(obj))
        else:
            messages.add_message(request, messages.WARNING, 'You don\'t have permission to edit this'.format(obj))
    else:
        messages.add_message(request, messages.ERROR, 'Something went wrong')
    return redirect('index')


@login_required
def delete_asset(request, asset_id):
    if request.method == 'POST':
        obj = get_object_or_404(Asset, pk=asset_id)
        if any([obj.user == request.user, request.user.is_superuser]):
            obj.delete()
            messages.add_message(request, messages.SUCCESS, 'Asset {} has been deleted successfully'.format(obj))
        else:
            messages.add_message(request, messages.WARNING, 'You don\'t have permission to edit this'.format(obj))
    else:
        messages.add_message(request, messages.ERROR, 'Something went wrong')
    return redirect('index')


@login_required
def create_project(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ProjectForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            new_project = form.save(commit=False)
            new_project.project_author = request.user
            new_project._change_reason = 'Project Created'
            new_project = form.save()
            messages.add_message(request, messages.SUCCESS, 'Project created successfully')
            return redirect('project', project_id=new_project.id)
        else:
            messages.add_message(request, messages.ERROR, 'Something went wrong')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = ProjectForm()

    return render(request, 'invmanager/forms/project_create.html', {'form': form})


@login_required
def create_note(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            new_note = form.save(commit=False)
            new_note.note_user = request.user
            new_note._change_reason = 'Note Created'
            new_note = form.save()
            messages.add_message(request, messages.SUCCESS, 'Note created successfully')
            return redirect('note', note_id=new_note.id)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    return render(request, 'invmanager/forms/note_create.html', {'form': form})


@login_required
def create_asset(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AssetForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            new_asset = form.save(commit=False)
            new_asset.user = request.user
            new_asset._change_reason = 'Note Created'
            new_asset = form.save()
            messages.add_message(request, messages.SUCCESS, 'Asset created successfully')
            return redirect('asset', asset_id=new_asset.id)
        else:
            messages.add_message(request, messages.ERROR, 'Something went wrong')
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AssetForm()

    return render(request, 'invmanager/forms/asset_create.html', {'form': form})


def signup(request):
    # TODO make email confirmation mandatory
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        profile_form = UserProfileForm(request.POST, request.FILES)
        invite_token = str(request.POST.get('invite_token', None))
        if not check_invite_token(request, invite_token):
            return redirect('index')
        if form.is_valid() and profile_form.is_valid():
            user = form.save()
            login(request, user)
            db_token = Invite.objects.get(token=invite_token)
            db_token.status = 'ACCEPTED'
            db_token.save()
            messages.add_message(request, messages.SUCCESS, 'profile created successfully, be sure to complete it')
            return redirect('index')
        else:
            messages.add_message(request, messages.ERROR,
                                 'Something went wrong creating the profile, is the form valid?')
            return redirect('index')
    elif request.method == 'GET':
        invite_token = request.GET.get('t', None)
        is_correct_token = check_invite_token(request, invite_token)
        if is_correct_token:
            form = UserCreationForm()
        else:
            return redirect('index')
    else:
        messages.add_message(request, messages.ERROR, 'wrong method used')
        return redirect('index')
    return render(request, 'registration/signup.html', {'form': form, 'invite_token': invite_token})
