# Generated by Django 3.2 on 2021-06-07 09:54

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('invmanager', '0025_auto_20210605_1449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 241943, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalasset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 241943, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalinventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 241943, tzinfo=utc), verbose_name='Date created'),
        ),
        migrations.AlterField(
            model_name='historicalproject',
            name='history_change_reason',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='historicalproject',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 195054, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='inventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 241943, tzinfo=utc), verbose_name='Date created'),
        ),
        migrations.AlterField(
            model_name='project',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 195054, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='workspace',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 7, 9, 54, 45, 241943, tzinfo=utc), verbose_name='Date started'),
        ),
    ]
