# Generated by Django 3.2 on 2021-06-10 09:51

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('invmanager', '0032_auto_20210610_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 52331, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalasset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 52331, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalinventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 53331, tzinfo=utc), verbose_name='Date created'),
        ),
        migrations.AlterField(
            model_name='historicalproject',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 50331, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='inventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 53331, tzinfo=utc), verbose_name='Date created'),
        ),
        migrations.AlterField(
            model_name='project',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 50331, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='workspace',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 10, 9, 51, 14, 54331, tzinfo=utc), verbose_name='Date started'),
        ),
    ]
