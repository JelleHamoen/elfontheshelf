# Generated by Django 3.2 on 2021-06-04 11:32

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('invmanager', '0020_auto_20210604_1318'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalinventorytool',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalinventorytool',
            name='tool_user',
        ),
        migrations.RemoveField(
            model_name='inventoryitem',
            name='item_user',
        ),
        migrations.RemoveField(
            model_name='inventoryitem',
            name='project',
        ),
        migrations.RemoveField(
            model_name='inventoryitem',
            name='tool',
        ),
        migrations.RemoveField(
            model_name='inventorytool',
            name='tool_user',
        ),
        migrations.RemoveField(
            model_name='historicalinventorynote',
            name='item',
        ),
        migrations.RemoveField(
            model_name='historicalinventorynote',
            name='tool',
        ),
        migrations.RemoveField(
            model_name='inventorynote',
            name='item',
        ),
        migrations.RemoveField(
            model_name='inventorynote',
            name='tool',
        ),
        migrations.AddField(
            model_name='historicalinventorynote',
            name='asset',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='invmanager.asset'),
        ),
        migrations.AddField(
            model_name='inventorynote',
            name='asset',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.RESTRICT, to='invmanager.asset'),
        ),
        migrations.AlterField(
            model_name='asset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 125843, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalasset',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 125843, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalinventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 127893, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='historicalproject',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 83842, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='inventorynote',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 127893, tzinfo=utc), verbose_name='Date acquired'),
        ),
        migrations.AlterField(
            model_name='project',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 83842, tzinfo=utc), verbose_name='Starting date'),
        ),
        migrations.AlterField(
            model_name='workspace',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 6, 4, 11, 32, 11, 128894, tzinfo=utc), verbose_name='Date started'),
        ),
        migrations.DeleteModel(
            name='HistoricalInventoryItem',
        ),
        migrations.DeleteModel(
            name='HistoricalInventoryTool',
        ),
        migrations.DeleteModel(
            name='InventoryItem',
        ),
        migrations.DeleteModel(
            name='InventoryTool',
        ),
    ]
