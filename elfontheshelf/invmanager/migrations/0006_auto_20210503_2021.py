# Generated by Django 3.2 on 2021-05-03 18:21

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('invmanager', '0005_auto_20210430_1612'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='associated_inventory_items',
        ),
        migrations.AlterField(
            model_name='project',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 5, 3, 18, 21, 23, 307539, tzinfo=utc), verbose_name='Starting date'),
        ),
    ]
