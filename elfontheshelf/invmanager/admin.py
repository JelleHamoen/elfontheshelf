from django.contrib import admin

from .models import Project, InventoryNote, Asset, UserProfile, Invite

admin.site.register(InventoryNote)
admin.site.register(Project)
admin.site.register(Asset)
admin.site.register(UserProfile)
admin.site.register(Invite)
