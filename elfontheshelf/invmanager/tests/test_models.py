from django.test import TestCase
from ..models import Asset, InventoryNote, Project, UserProfile, split_db_tags, Invite
from django.contrib.auth.models import User
from django.test.client import Client
from django.core.validators import URLValidator
from django.utils import timezone
import datetime


class AssetModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        c = Client()
        user = User.objects.create_user(username='testuser', password='12345')
        c.login(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods
        cls.asset = Asset.objects.create(user=user, title='model test', category='tool', status='testing',
                                         raw_tags='test, another')

    def test_tag_split(self):
        split_tags = self.asset.tags()
        self.assertEqual(split_tags, ['test', 'another'])

    def test_img_url(self):
        img_url = self.asset.img_url()
        self.assertEqual(img_url, '/media/default128.png')

    def test_str(self):
        self.assertEqual(str(self.asset), 'model test')


class ProjectModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        c = Client()
        user = User.objects.create_user(username='testuser', password='12345')
        c.login(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods
        cls.project = Project.objects.create(project_author=user, project_title='model test', status='testing',
                                             project_tags='test, another')

    #     TODO add visibility integration testing

    def test_str(self):
        self.assertEqual(str(self.project), 'model test')

    def test_tag_split(self):
        split_tags = self.project.tags()
        self.assertEqual(split_tags, ['test', 'another'])

    def test_img_url(self):
        img_url = self.project.img_url()
        self.assertEqual(img_url, '/media/default128.png')


class NoteModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        c = Client()
        user = User.objects.create_user(username='testuser', password='12345')
        c.login(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods
        cls.note = InventoryNote.objects.create(note_user=user, note_text='testing the note text',
                                                status='testing', note_tags='test, another')

    def test_tag_split(self):
        split_tags = self.note.tags()
        self.assertEqual(split_tags, ['test', 'another'])


class UserProfileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user.save()
        cls.user.userprofile.website = 'http://test.dev'
        cls.user.save()
        c.login(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_if_userprofile_created(self):
        # both the user obj and userprofile
        profile = UserProfile.objects.get(user=self.user)
        self.assertEqual(profile.website, self.user.userprofile.website)

    def test_if_website_valid(self):
        website = self.user.userprofile.website
        self.assertTrue(URLValidator(message=website))

    def test_img_url(self):
        img_url = self.user.userprofile.img_url()
        self.assertEqual(img_url, '/media/default128.png')

    def test_str(self):
        self.assertEqual(str(self.user.userprofile), self.user.username)


class SplitDbTagsModelTest(TestCase):
    def test_if_single_tag_not_split(self):
        split = split_db_tags('one')
        self.assertEqual(split, ['one'])

    def test_if_empty_returns_none(self):
        is_none = split_db_tags('')
        self.assertIsNone(is_none)


class InviteModelTests(TestCase):

    def test_is_valid_with_future_date(self):
        time = timezone.now() + datetime.timedelta(days=40)
        invite = Invite(issued=time)
        self.assertIs(invite.is_expired(), False)
