from django.test import TestCase
from ..models import Asset, InventoryNote, Project, UserProfile, split_db_tags, Invite
from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.test.client import Client
from django.urls import reverse
from ..views import index
from model_bakery import baker
import datetime


class IndexViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_non_auth_template(self):
        response = self.c.get(reverse('index'))
        self.assertTemplateUsed(response, 'invmanager/pages/index.html')

    def test_auth(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/listall.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Index')

    def test_no_tiles(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('index'))
        self.assertQuerysetEqual(response.context['tiles'], [])

    def test_one_of_each_tiles(self):
        self.c.login(username='testuser', password='12345')
        project = baker.make('Project', project_author=self.user, project_description='test')
        note = baker.make('InventoryNote', project=project, note_user=self.user, note_text='testing')
        asset = baker.make('Asset', user=self.user, description='test')
        response = self.c.get(reverse('index'))
        self.assertEqual(len(response.context['tiles']), 3)
        # Test if correct items have been retrieved
        for item, tile_type in response.context['tiles']:
            if tile_type == 'Project':
                self.assertEqual(item, project)
            elif tile_type == 'Asset':
                self.assertEqual(item, asset)
            elif tile_type == 'InventoryNote':
                self.assertEqual(item, note)
            else:
                raise ValueError('Unknown tile type')


class ProjectListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('projectlist'))
        self.assertRedirects(response, '/accounts/login/?next=%2Fproject%2F')

    def test_auth_empty(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('projectlist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/projectlist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Projects')
        self.assertQuerysetEqual(response.context['latest_project_list'], [])
        self.assertContains(response, 'No projects are available.')

    def test_auth_filled(self):
        self.c.login(username='testuser', password='12345')
        project1 = baker.make('Project', project_author=self.user, project_description='test1')
        project2 = baker.make('Project', project_author=self.user, project_description='test2')
        project3 = baker.make('Project', project_author=self.user, project_description='test3')
        response = self.c.get(reverse('projectlist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/projectlist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Projects')
        self.assertTrue(all(x in [project1, project2, project3] for x in list(response.context['latest_project_list'])))


class AssetListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('assetlist'))
        self.assertRedirects(response, '/accounts/login/?next=%2Fasset%2F')

    def test_auth_empty(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('assetlist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/assetlist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Assets')
        self.assertQuerysetEqual(response.context['latest_asset_list'], [])
        self.assertContains(response, 'No assets are available.')

    def test_auth_filled(self):
        self.c.login(username='testuser', password='12345')
        asset1 = baker.make('Asset', user=self.user, description='test1')
        asset2 = baker.make('Asset', user=self.user, description='test2')
        asset3 = baker.make('Asset', user=self.user, description='test3')
        response = self.c.get(reverse('assetlist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/assetlist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Assets')
        self.assertTrue(all(x in [asset1, asset2, asset3] for x in list(response.context['latest_asset_list'])))


class NoteListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('notelist'))
        self.assertRedirects(response, '/accounts/login/?next=%2Fnote%2F')

    def test_auth_empty(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('notelist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/notelist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Notes')
        self.assertQuerysetEqual(response.context['latest_note_list'], [])
        self.assertContains(response, 'No notes are available.')

    def test_auth_filled(self):
        self.c.login(username='testuser', password='12345')
        note1 = baker.make('InventoryNote', note_user=self.user, note_text='test1')
        note2 = baker.make('InventoryNote', note_user=self.user, note_text='test2')
        note3 = baker.make('InventoryNote', note_user=self.user, note_text='test3')
        response = self.c.get(reverse('notelist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/notelist.html')
        self.assertEqual(response.context['search'], True)
        self.assertEqual(response.context['title'], 'Beunbuddy| Notes')
        self.assertTrue(all(x in [note1, note2, note3] for x in list(response.context['latest_note_list'])))


class ProfileViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        # Set up non-modified objects used by all test methods

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('profile'))
        self.assertRedirects(response, '/accounts/login/?next=%2Fprofile%2F')

    def test_auth_load(self):
        self.c.login(username='testuser', password='12345')
        self.user.userprofile.website = 'http://test.dev'
        self.user.save()
        response = self.c.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/profile.html')
        self.assertEqual(response.context['title'], 'Beunbuddy| Profile')
        self.assertContains(response, 'estuser')
        self.assertContains(response, 'http://test.dev')


#     TODO check if thumbnail loads correctly -> probably implement with browser testing


class AssetPageViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        # Set up non-modified objects used by all test methods
        cls.asset = baker.make('Asset', user=cls.user, description='test')

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('asset', kwargs={'asset_id': self.asset.id}))
        self.assertRedirects(response, '/accounts/login/?next=%2Fasset%2F{}%2F'.format(self.asset.id))

    def test_404(self):
        self.c.login(username='testuser', password='12345')
        # get non existent asset, should return 404
        response = self.c.get(reverse('asset', args=[69]))
        self.assertEqual(response.status_code, 404)

    def test_auth_load_without_project(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('asset', args=[self.asset.id]))
        # print(response['location'])
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/assetpage.html')
        self.assertIsNone(response.context['project'])
        self.assertEqual(response.context['asset'], self.asset)
        self.assertContains(response, self.asset.description)

    def test_wrong_user(self):
        # this user should not be able to see the page
        self.c.login(username='maluser', password='12345')
        response = self.c.get(reverse('asset', kwargs={'asset_id': self.asset.id}))
        # self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('index'))

    def test_admin_user(self):
        # this user should be able to see the page
        self.c.login(username='admin', password='12345')
        response = self.c.get(reverse('asset', kwargs={'asset_id': self.asset.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/assetpage.html')


class NotePageViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        # Set up non-modified objects used by all test methods
        cls.note = baker.make('InventoryNote', note_user=cls.user, note_text='test', note_tags='een,twee')

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('note', kwargs={'note_id': self.note.id}))
        self.assertRedirects(response, '/accounts/login/?next=%2Fnote%2F{}%2F'.format(self.note.id))

    def test_404(self):
        self.c.login(username='testuser', password='12345')
        # get non existent asset, should return 404
        response = self.c.get(reverse('note', args=[69]))
        self.assertEqual(response.status_code, 404)

    def test_auth_load(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/notepage.html')
        self.assertEqual(response.context['note'], self.note)
        self.assertContains(response, self.note.note_text)

    def test_wrong_user(self):
        # this user should not be able to see the page
        self.c.login(username='maluser', password='12345')
        response = self.c.get(reverse('note', kwargs={'note_id': self.note.id}))
        # self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('index'))

    def test_admin_user(self):
        # this user should be able to see the page
        self.c.login(username='admin', password='12345')
        response = self.c.get(reverse('note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/notepage.html')


class ProjectPageViewTest(TestCase):
    # TODO add project assets and note and check proliferation
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        # Set up non-modified objects used by all test methods
        cls.project = Project.objects.create(project_author=cls.user, project_title='model test', status='testing',
                                             project_tags='test, another')

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('project', kwargs={'project_id': self.project.id}))
        self.assertRedirects(response, '/accounts/login/?next=%2Fproject%2F{}%2F'.format(self.project.id))

    def test_404(self):
        self.c.login(username='testuser', password='12345')
        # get non existent asset, should return 404
        response = self.c.get(reverse('project', args=[69]))
        self.assertEqual(response.status_code, 404)

    def test_auth_load(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/projectpage.html')
        self.assertEqual(response.context['project'], self.project)
        self.assertContains(response, self.project.project_description)

    def test_wrong_user(self):
        # this user should not be able to see the page
        self.c.login(username='maluser', password='12345')
        response = self.c.get(reverse('project', kwargs={'project_id': self.project.id}))
        self.assertRedirects(response, reverse('index'))

    def test_admin_user(self):
        # this user should be able to see the page
        self.c.login(username='admin', password='12345')
        response = self.c.get(reverse('project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/pages/projectpage.html')


class TagPageViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        cls.project = Project.objects.create(project_author=cls.user, project_title='model test', status='testing',
                                             project_tags='test, another')
        cls.note = baker.make('InventoryNote', project=cls.project, note_user=cls.user, note_text='testing',
                              note_tags='test, else')
        cls.asset = baker.make('Asset', user=cls.user, description='test', raw_tags='test, bla')
        cls.asset2 = baker.make('Asset', user=cls.user2, description='test2', raw_tags='test, bla')

    def test_non_auth_redirect(self):
        response = self.c.get(reverse('tag', kwargs={'tag': 'test'}))
        self.assertRedirects(response, '/accounts/login/?next=/tag/test/')

    def test_tag_view_auth_one_of_each_tile(self):
        self.c.login(username='testuser', password='12345')
        response = self.c.get(reverse('tag', kwargs={'tag': 'test'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/listall.html')
        self.assertEqual(response.context['title'], 'Beunbuddy| test')
        self.assertTrue(response.context['search'])
        self.assertEqual(len(response.context['tiles']), 3)
        for item, tile_type in response.context['tiles']:
            # tag page should only have the three tiles
            if tile_type == 'Project':
                self.assertEqual(item, self.project)
            elif tile_type == 'Asset':
                self.assertEqual(item, self.asset)
            elif tile_type == 'InventoryNote':
                self.assertEqual(item, self.note)
            else:
                raise ValueError('Unknown tile')

    def test_tag_view_auth_other_user(self):
        self.c.login(username='maluser', password='12345')
        response = self.c.get(reverse('tag', kwargs={'tag': 'test'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/listall.html')
        self.assertEqual(response.context['title'], 'Beunbuddy| test')
        self.assertTrue(response.context['search'])
        self.assertEqual(len(response.context['tiles']), 1)

    def test_tag_view_auth_empty(self):
        self.c.login(username='maluser', password='12345')
        self.asset2.delete()
        response = self.c.get(reverse('tag', kwargs={'tag': 'test'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'invmanager/lists/listall.html')
        self.assertEqual(response.context['title'], 'Beunbuddy| test')
        self.assertTrue(response.context['search'])
        self.assertContains(response, 'No items are available.')


class SignupPageTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        # Invite issued 3 days ago
        issued_date = datetime.datetime.now() - datetime.timedelta(days=3)
        cls.invite = Invite.objects.create()
        cls.invite.issued = issued_date
        cls.invite.save()
        # Invite issued more than 30 days ago
        expired_issued_date = datetime.datetime.now() - datetime.timedelta(days=33)
        cls.expired_invite = Invite.objects.create()
        cls.invite.issued = expired_issued_date
        cls.invite.save()

    def test_non_token_redirect(self):
        response = self.c.get(reverse('signup'))
        self.assertRedirects(response, '/')

    def test_invalid_token_redirect(self):
        invalid_token = 'test'
        response = self.c.get(reverse('signup') + '?t={}'.format(invalid_token))
        self.assertRedirects(response, '/')

    def test_expired_token_redirect(self):
        expired_token = self.expired_invite.token
        response = self.c.get(reverse('signup') + '?t={}'.format(expired_token))
        self.assertRedirects(response, '/')

    def test_correct_token_load(self):
        correct_token = self.invite.token
        response = self.c.get(reverse('signup') + '?t={}'.format(correct_token))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/signup.html')
        self.assertEqual(response.context['invite_token'], correct_token)
        self.assertContains(response, correct_token)


#     TODO test actual creation


class DeleteProjectTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        cls.project = Project.objects.create(project_author=cls.user, project_title='model test', status='testing',
                                             project_tags='test, another')

    def test_delete_non_auth(self):
        # non auth should return a redirect and not delete the project
        response = self.c.post(reverse('delete_project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Project.objects.get(pk=self.project.id), self.project)

    def test_delete_404_project(self):
        # deleting a project that does not exist should redirect to 404
        response = self.c.post(reverse('delete_project', kwargs={'project_id': 69}))
        self.assertEqual(response.status_code, 302)

    def test_delete_wrong_auth(self):
        # A user who doesnt own the project should not be able to delete it
        self.c.login(username='maluser', password='12345')
        response = self.c.post(reverse('delete_project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Project.objects.get(pk=self.project.id), self.project)
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('permission'))

    def test_delete_auth(self):
        # The correct user should be able to delete the project
        self.c.login(username='testuser', password='12345')
        self.assertEqual(Project.objects.get(pk=self.project.id), self.project)
        response = self.c.post(reverse('delete_project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Project.objects.filter(pk=self.project.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))

    def test_delete_admin(self):
        # An admin should be able to delete a project
        self.c.login(username='admin', password='12345')
        self.assertEqual(Project.objects.get(pk=self.project.id), self.project)
        response = self.c.post(reverse('delete_project', kwargs={'project_id': self.project.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Project.objects.filter(pk=self.project.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))


class DeleteNoteTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        cls.note = baker.make('InventoryNote', note_user=cls.user, note_text='test', note_tags='een,twee')

    def test_delete_non_auth(self):
        # non auth should return a redirect and not delete the note
        response = self.c.post(reverse('delete_note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InventoryNote.objects.get(pk=self.note.id), self.note)

    def test_delete_404_project(self):
        # deleting a note that does not exist should redirect to 404
        response = self.c.post(reverse('delete_note', kwargs={'note_id': 69}))
        self.assertEqual(response.status_code, 302)

    def test_delete_wrong_auth(self):
        # A user who doesnt own the note should not be able to delete it
        self.c.login(username='maluser', password='12345')
        response = self.c.post(reverse('delete_note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(InventoryNote.objects.filter(pk=self.note.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('permission'))

    def test_delete_auth(self):
        # The correct user should be able to delete the note
        self.c.login(username='testuser', password='12345')
        self.assertEqual(InventoryNote.objects.get(pk=self.note.id), self.note)
        response = self.c.post(reverse('delete_note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(InventoryNote.objects.filter(pk=self.note.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))

    def test_delete_admin(self):
        # An admin should be able to delete a note
        self.c.login(username='admin', password='12345')
        self.assertEqual(InventoryNote.objects.get(pk=self.note.id), self.note)
        response = self.c.post(reverse('delete_note', kwargs={'note_id': self.note.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(InventoryNote.objects.filter(pk=self.note.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))


class DeleteAssetTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.c = Client()
        cls.user = User.objects.create_user(username='testuser', password='12345')
        cls.user2 = User.objects.create_user(username='maluser', password='12345')
        cls.admin = User.objects.create_superuser(username='admin', password='12345')
        cls.asset = baker.make('Asset', user=cls.user, description='test', raw_tags='test, bla')

    def test_delete_non_auth(self):
        # non auth should return a redirect and not delete the asset
        response = self.c.post(reverse('delete_asset', kwargs={'asset_id': self.asset.id}))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Asset.objects.get(pk=self.asset.id), self.asset)

    def test_delete_404_project(self):
        # deleting a asset that does not exist should redirect to 404
        response = self.c.post(reverse('delete_asset', kwargs={'asset_id': 69}))
        self.assertEqual(response.status_code, 302)

    def test_delete_wrong_auth(self):
        # A user who doesnt own the asset should not be able to delete it
        self.c.login(username='maluser', password='12345')
        response = self.c.post(reverse('delete_asset', kwargs={'asset_id': self.asset.id}))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Asset.objects.filter(pk=self.asset.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('permission'))

    def test_delete_auth(self):
        # The correct user should be able to delete the asset
        self.c.login(username='testuser', password='12345')
        self.assertEqual(Asset.objects.get(pk=self.asset.id), self.asset)
        response = self.c.post(reverse('delete_asset', kwargs={'asset_id': self.asset.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Asset.objects.filter(pk=self.asset.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))

    def test_delete_admin(self):
        # An admin should be able to delete a asset
        self.c.login(username='admin', password='12345')
        self.assertEqual(Asset.objects.get(pk=self.asset.id), self.asset)
        response = self.c.post(reverse('delete_asset', kwargs={'asset_id': self.asset.id}))
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Asset.objects.filter(pk=self.asset.id).exists())
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertTrue(messages[0].message.find('successfully'))
