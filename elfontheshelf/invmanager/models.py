from django.db import models
from django.utils import timezone
from django.conf import settings
from django.forms import ModelForm
from simple_history.models import HistoricalRecords
from django import forms
from ckeditor.fields import RichTextField
import datetime
import re
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import secrets

# Create your models here.

VISIBILITY_CHOICES = (
    ('public', 'PUBLIC'),
    ('private', 'PRIVATE'),
    ('unlisted', 'UNLISTED')
)

CATEGORY_CHOICES = (
    ('tool', 'TOOL'),
    ('consumable', 'CONSUMABLE'),
    ('material', 'MATERIAL'),
)

INVITE_CHOICES = {
    ('pending', 'PENDING'),
    ('accepted', 'ACCEPTED'),
    ('expired', 'EXPIRED'),
}


def split_db_tags(tag_string):
    if not tag_string:
        return None
    # remove non-word characters from db string
    tag_string = re.sub(r'[^\w,]+', '', tag_string)
    if ',' in tag_string:
        tags = tag_string.split(',')
        if len(tags) < 2:
            tags = None
        return tags
    else:
        return [tag_string]


class Project(models.Model):
    project_title = models.CharField(max_length=200)
    project_description = RichTextField(max_length=1000)
    # https://docs.djangoproject.com/en/3.2/ref/models/fields/#django.db.models.JSONField
    pub_date = models.DateTimeField('Starting date', default=timezone.now)
    status = models.CharField(max_length=200)
    project_author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    project_tags = models.CharField(max_length=200, blank=True)
    tile_width = models.IntegerField(default=4)
    thumbnail = models.ImageField(blank=True, upload_to='project/thumbnail', default='default128.png')
    visibility = models.CharField(max_length=60, choices=VISIBILITY_CHOICES, default='private')
    history = HistoricalRecords(history_change_reason_field=models.TextField(null=True),
                                excluded_fields=['visibility', 'thumbnail', 'tile_width',
                                                 'project_description', 'project_title'])
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.project_title

    def tags(self):
        return split_db_tags(self.project_tags)

    def img_url(self):
        # Get image from model, or return default image if not present
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return '/media/default128.png'


class Asset(models.Model):
    title = models.CharField(max_length=200)
    category = models.CharField(max_length=200, choices=CATEGORY_CHOICES, blank=True, null=True)
    description = RichTextField(max_length=1000)
    pub_date = models.DateTimeField('Date acquired', default=timezone.now)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
    status = models.CharField(max_length=200, blank=True)
    history = HistoricalRecords()
    raw_tags = models.CharField(max_length=200, blank=True)
    last_modified = models.DateTimeField(auto_now=True)
    thumbnail = models.ImageField(blank=True, upload_to='asset/thumbnail', default='default128.png')

    def __str__(self):
        return self.title

    def tags(self):
        return split_db_tags(self.raw_tags)

    def img_url(self):
        # Get image from model, or return default image if not present
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return '/media/default128.png'


class InventoryNote(models.Model):
    note_text = RichTextField(max_length=1000)
    pub_date = models.DateTimeField('Date created', default=timezone.now)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=True, null=True)
    asset = models.ForeignKey(Asset, on_delete=models.SET_NULL, blank=True, null=True)
    note_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
    note_tags = models.CharField(max_length=200, blank=True)
    tile_width = models.IntegerField(default=4)
    status = models.CharField(max_length=200, blank=True)
    history = HistoricalRecords()
    last_modified = models.DateTimeField(auto_now=True)

    def tags(self):
        return split_db_tags(self.note_tags)

    def __str__(self):
        return 'note created at {}'.format(self.pub_date)


class Workspace(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Date started', default=timezone.now)
    thumbnail = models.ImageField(blank=True, upload_to='workspace/thumbnail')
    visibility = models.CharField(max_length=60, choices=VISIBILITY_CHOICES, default='private')
    last_modified = models.DateTimeField(auto_now=True)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    website = models.URLField(blank=True)
    thumbnail = models.ImageField(upload_to="profile_images", blank=True, null=True)

    def __str__(self):
        return self.user.username

    def img_url(self):
        # Get image from model, or return default image if not present
        if self.thumbnail and hasattr(self.thumbnail, 'url'):
            return self.thumbnail.url
        else:
            return '/media/default128.png'


def generate_token():
    token = secrets.token_urlsafe()
    return token


class Invite(models.Model):
    token = models.CharField(max_length=200, default=generate_token)
    status = models.CharField(max_length=60, choices=INVITE_CHOICES, default='PENDING')
    issued = models.DateTimeField(auto_now_add=True)

    def is_expired(self):
        now = timezone.now()
        return now - datetime.timedelta(days=30) <= self.issued <= now

    def __str__(self):
        return '{1} invite issued {0}'.format(self.issued,  self.status)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()
