from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('profile/', views.profile, name='profile'),
    path('profile/edit', views.edit_profile, name='edit_profile'),
    path('project/', views.projectlist, name='projectlist'),
    path('project/create/', views.create_project, name='create_project'),
    path('project/<int:project_id>/', views.project_page, name='project'),
    path('project/<int:project_id>/edit', views.edit_project, name='edit_project'),
    path('project/<int:project_id>/delete', views.delete_project, name='delete_project'),
    path('asset/', views.assetlist, name='assetlist'),
    path('asset/create/', views.create_asset, name='create_asset'),
    path('asset/<int:asset_id>/', views.asset_page, name='asset'),
    path('asset/<int:asset_id>/edit', views.edit_asset, name='edit_asset'),
    path('asset/<int:asset_id>/delete', views.delete_asset, name='delete_asset'),
    path('note/', views.notelist, name='notelist'),
    path('note/<int:note_id>/', views.note_page, name='note'),
    path('note/<int:note_id>/edit', views.edit_note, name='edit_note'),
    path('note/<int:note_id>/delete', views.delete_note, name='delete_note'),
    path('note/create/', views.create_note, name='create_note'),
    path('tag/<str:tag>/', views.tag_page, name='tag'),

]
