$(document).ready(function () {
    $("#searchbar").submit(function (e) {
        e.preventDefault();
    });
});

function filter_input(user_input) {
//    TODO sanitize input
    let children = document.getElementById('tileList').children;
    for (let i = 0; i < children.length; i++) {
        let tile = children[i];
        if (tile.innerHTML.indexOf(user_input.toString()) == -1) {
            tile.classList.add('d-none')
        } else {
            tile.classList.remove('d-none')
        }
    }
    return false;
}